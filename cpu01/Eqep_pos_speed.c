//###########################################################################
//
// FILE:    Eqep_pos_speed.c
//
// TITLE:    EQEP Speed and Position measurement
//
//! \addtogroup cpu01_example_list
//! <h1>EQEP Speed and Position Measurement (Eqep_pos_speed)</h1>
//!
//! This example provides position measurement,speed measurement using the
//! capture unit, and speed measurement using unit time out. This example
//! uses the IQMath library. It is used merely to simplify high-precision
//! calculations.
//! The example requires the following hardware connections from EPWM1 and
//! GPIO pins (simulating QEP sensor) to QEP peripheral.
//!    - GPIO20/eQEP1A <- GPIO0/ePWM1A (simulates eQEP Phase A signal)
//!    - GPIO21/eQEP1B <- GPIO1/ePWM1B (simulates eQEP Phase B signal)
//!    - GPIO23/eQEP1I <- GPIO4 (simulates eQEP Index Signal)
//!
//! See DESCRIPTION in Example_posspeed.c for more details on the calculations
//! performed in this example.
//! In addition to this file, the following files must be included in this
//! project:
//!    - Example_posspeed.c - includes all eQEP functions
//!    - Example_EPwmSetup.c - sets up ePWM1A and ePWM1B as simulated QA and QB
//!                            encoder signals
//!    - Example_posspeed.h - includes initialization values for pos and speed
//!                           structure
//!
//! Note:
//!    - Maximum speed is configured to 6000rpm(BaseRpm)
//!    - Minimum speed is assumed at 10rpm for capture pre-scalar selection
//!    - Pole pair is configured to 2 (pole_pairs)
//!    - QEP Encoder resolution is configured to 4000counts/revolution
//!      (mech_scaler)
//!    - Which means: 4000/4 = 1000 line/revolution quadrature encoder
//!      (simulated by EPWM1)
//!    - EPWM1 (simulating QEP encoder signals) is configured for 5kHz
//!      frequency or 300 rpm (=4*5000 cnts/sec * 60 sec/min)/4000 cnts/rev)
//!    - SPEEDRPM_FR: High Speed Measurement is obtained by counting the QEP
//!                   input pulses for 10ms (unit timer set to 100Hz).
//!    - SPEEDRPM_FR = (Position Delta/10ms) * 60 rpm
//!    - SPEEDRPM_PR: Low Speed Measurement is obtained by measuring time
//!                   period of QEP edges. Time measurement is averaged over
//!                   64edges for better results and capture unit performs the
//!                   time measurement using pre-scaled SYSCLK
//!    - Pre-scaler for capture unit clock is selected such that capture timer
//!      does not overflow at the required minimum RPM speed.
//!
//! \b External \b Connections \n
//!   - Connect eQEP1A(GPIO20) to ePWM1A(GPIO0)(simulates eQEP Phase A signal)
//!   - Connect eQEP1B(GPIO21) to ePWM1B(GPIO1)(simulates eQEP Phase B signal)
//!   - Connect eQEP1I(GPIO23) to GPIO4 (simulates eQEP Index Signal)
//!
//! \b Watch \b Variables \n
//!  - qep_posspeed.SpeedRpm_fr - Speed meas. in rpm using QEP position counter
//!  - qep_posspeed.SpeedRpm_pr - Speed meas. in rpm using capture unit
//!  - qep_posspeed.theta_mech  - Motor mechanical angle (Q15)
//!  - qep_posspeed.theta_elec  - Motor electrical angle (Q15)
//!
//
//###########################################################################
// $TI Release: F2837xD Support Library v3.07.00.00 $
// $Release Date: Sun Sep 29 07:34:54 CDT 2019 $
// $Copyright:
// Copyright (C) 2013-2019 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

//
// Included Files
//
#include "F28x_Project.h"
#include "Example_posspeed.h"
//#include <stdio.h>
//#include <string.h>


#define REFERENCE_VDAC      0
#define REFERENCE_VREF      1
#define DACA         1
#define DACB         2
#define DACC         3
#define REFERENCE            REFERENCE_VREF
#define DAC_NUM                DACA

//
// Globals
//
POSSPEED qep_posspeed=POSSPEED_DEFAULTS;
Uint16 Interrupt_Count = 0;
Uint16 LoopCount;

volatile struct DAC_REGS* DAC_PTR[4] = {0x0,&DacaRegs,&DacbRegs,&DaccRegs};
Uint16 dacval = 2048;

//
// Function Prototypes
//
void initEpwm();
__interrupt void prdTick(void);
void scia_echoback_init(void);
void scia_fifo_init(void);
void scia_xmit(int a);
void scia_msg(char *msg);
void scia_16bit(uint16_t data);

void configureDAC(Uint16 dac_num);


//
// Main
//
int send_speed = 0;
int send_pos = 0;
int on_theta = 0;

void main(void)
{
    Uint16 ReceivedChar;
    char *msg;
    int msg_count = 0;


//
// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the F2837xD_SysCtrl.c file.
//
   InitSysCtrl();

//
// Step 2. Initialize GPIO:
// This example function is found in the F2837xD_Gpio.c file and
// illustrates how to set the GPIO to its default state.
//
// InitGpio();  // Skipped for this example

//
// For this case only init GPIO for eQEP1 and ePWM1
// This function is found in F2837xD_EQep.c
//
   InitGpio();
   InitEQep1Gpio();
   InitEPwm1Gpio();
   EALLOW;
   GpioCtrlRegs.GPADIR.bit.GPIO4 = 1; // GPIO4 as output simulates Index signal
   GpioDataRegs.GPACLEAR.bit.GPIO4 = 1;  // Normally low
   EDIS;

   GPIO_SetupPinMux(28, GPIO_MUX_CPU1, 1);
   GPIO_SetupPinOptions(28, GPIO_INPUT, GPIO_PUSHPULL);
   GPIO_SetupPinMux(29, GPIO_MUX_CPU1, 1);
   GPIO_SetupPinOptions(29, GPIO_OUTPUT, GPIO_ASYNC);

//
// Step 3. Clear all __interrupts and initialize PIE vector table:
// Disable CPU __interrupts
//
   DINT;

//
// Initialize the PIE control registers to their default state.
// The default state is all PIE __interrupts disabled and flags
// are cleared.
// This function is found in the F2837xD_PieCtrl.c file.
//
   InitPieCtrl();

//
// Disable CPU __interrupts and clear all CPU __interrupt flags:
//
   IER = 0x0000;
   IFR = 0x0000;

//
// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the __interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in F2837xD_DefaultIsr.c.
// This function is found in F2837xD_PieVect.c.
//
   InitPieVectTable();


   configureDAC(DAC_NUM);

//
// Interrupts that are used in this example are re-mapped to
// ISR functions found within this file.
//
   EALLOW;  // This is needed to write to EALLOW protected registers
   PieVectTable.EPWM1_INT= &prdTick;
   EDIS;    // This is needed to disable write to EALLOW protected registers

//
// Step 4. Initialize all the Device Peripherals:
//
   initEpwm();  // This function exists in Example_EPwmSetup.c

//
// Step 5. User specific code, enable __interrupts:
// Enable CPU INT1 which is connected to CPU-Timer 0:
//
   IER |= M_INT3;

//
// Enable TINT0 in the PIE: Group 3 __interrupt 1
//
   PieCtrlRegs.PIEIER3.bit.INTx1 = 1;

//
// Enable global Interrupts and higher priority real-time debug events:
//
   EINT;   // Enable Global __interrupt INTM
   ERTM;   // Enable Global realtime __interrupt DBGM

   qep_posspeed.init(&qep_posspeed);

   LoopCount = 0;

   scia_fifo_init();       // Initialize the SCI FIFO
   scia_echoback_init();   // Initialize SCI for echoback

   msg = "\r\n\n\nHello World!\0";
   scia_msg(msg);

   msg = "\r\nYou will enter a character, and the DSP will echo it back! \n\0";
   scia_msg(msg);

   for(;;)
   {

      while(SciaRegs.SCIFFRX.bit.RXFFST == 0) { } // wait for empty state
      ReceivedChar = SciaRegs.SCIRXBUF.all;
      scia_xmit(ReceivedChar);
      msg[msg_count] = ReceivedChar;
      msg_count++;

      if (ReceivedChar == 'a'){
          on_theta += 100;
//              sprintf(msg, "on_theta in", on_theta);
          scia_msg("\n\r+100\n\r");
      }

      if (ReceivedChar == 's'){
          on_theta -= 100;
//              sprintf(msg, "on_theta: %d\n", on_theta);
          scia_msg("\n\r-100\n\r");
      }

      if (ReceivedChar == 'd'){
          on_theta += 10;
//              sprintf(msg, "on_theta: %d\n", on_theta);
          scia_msg("\n\r+10\n\r");

          scia_msg(msg);
      }

      if (ReceivedChar == 'f'){
          on_theta -= 10;
//              sprintf(msg, "on_theta: %d\n", on_theta);
          scia_msg("\n\r-10\n\r");
      }

//      if (ReceivedChar == '\r')
//      {
//          msg[msg_count] = '\0';
//          msg_count = 0;
//
//          char * cmd;
//          char *cmd_split = " \r\n";
//
//
//          cmd = strtok(msg, cmd_split);
//          val1 = strtok(NULL, cmd_split);
//          val2 = strtok(NULL, cmd_split);
//          val3 = strtok(NULL, cmd_split);


//          scia_msg("\r\n");

//          else{
//              sprintf(msg, "invalid command\r\n", (int) qep_posspeed.SpeedRpm_fr);
//              scia_msg(msg);
//          }
//      }

      LoopCount++;
   }
}

//
// prdTick - EPWM1 Interrupts once every 4 QCLK counts (one period)
//

float theta = 0.0;

__interrupt void prdTick(void)
{
//    Uint16 i;

   //
   // Position and Speed measurement
   //
    qep_posspeed.calc(&qep_posspeed);
    theta = -qep_posspeed.theta_raw;

   //Pulse GPIO4

    if (theta >= on_theta && theta < on_theta + 1000)
        GpioDataRegs.GPADAT.bit.GPIO4 = 0;
    else
        GpioDataRegs.GPADAT.bit.GPIO4 = 1;


    qep_posspeed.calc(&qep_posspeed);
    GpioDataRegs.GPADAT.bit.GPIO31 = 0;


    ///


   //DAC_Loop

    float dac_val = 0;
    float delta_dac = 0;

    float rot_per_scan = 5.0; // lines is 4x this value due to number of facets
    int rot_count = 0;

    int theta_prev = 0;
    float delta_theta = 0.0;

   //Detect theta wrap around and calculate the change in theta
   if (theta < theta_prev){
       rot_count++;
       delta_theta = (4000 - theta_prev) + qep_posspeed.theta_raw;
   }
   else{
       delta_theta = theta - theta_prev;
   }

   delta_dac = delta_theta / 4000. * 4096. /  rot_per_scan;
   // reset dac val after 2 scans (up and down)
   if (rot_count == (rot_per_scan * 2))
   {
       rot_count =  0;
   }

   //reset dac_val on beginning of each ramp up to keep phase.
   //I expect some drift from rounding errors
   if (rot_count == 0)
   {
       dac_val = theta / 4000.0 * 4096.0 / rot_per_scan;
   }
   else{
       dac_val += (delta_dac);
   }

   if (dac_val < 0)
       DAC_PTR[DAC_NUM]->DACVALS.all = 0;
   else if (dac_val < 4095)
       DAC_PTR[DAC_NUM]->DACVALS.all = dac_val;
   else if (dac_val < 8191)
       DAC_PTR[DAC_NUM]->DACVALS.all = 8191 - dac_val;
   else if (dac_val > 8191)
       DAC_PTR[DAC_NUM]->DACVALS.all = 0;

   theta_prev = theta;


//   Interrupt_Count++;
//   if (Interrupt_Count==16) // loop runs at 8k so this slows down data output
//   {
//       Interrupt_Count = 0;               // Reset count
//       if(send_speed)
//       {
//           //!  - qep_posspeed.SpeedRpm_fr - Speed meas. in rpm using QEP position counter
//           //!  - qep_posspeed.SpeedRpm_pr - Speed meas. in rpm using capture unit
//           //!  - It seems that Speed_fr = speed_rpm * 2795
//               scia_16bit((-1 * qep_posspeed.SpeedRpm_fr));
//               scia_xmit('\n');
//       }
//       if(send_pos)
//       {
//               scia_16bit((-1 * qep_posspeed.theta_raw));
//               scia_xmit('\n');
//       }
//
//   }

   //
   // Acknowledge this __interrupt to receive more __interrupts from group 1
   //
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
   EPwm1Regs.ETCLR.bit.INT=1;
}

void scia_echoback_init()
{
    //
    // Note: Clocks were turned on to the SCIA peripheral
    // in the InitSysCtrl() function
    //

    SciaRegs.SCICCR.all = 0x0007;   // 1 stop bit,  No loopback
                                    // No parity,8 char bits,
                                    // async mode, idle-line protocol
    SciaRegs.SCICTL1.all = 0x0003;  // enable TX, RX, internal SCICLK,
                                    // Disable RX ERR, SLEEP, TXWAKE
    SciaRegs.SCICTL2.all = 0x0003;
    SciaRegs.SCICTL2.bit.TXINTENA = 1;
    SciaRegs.SCICTL2.bit.RXBKINTENA = 1;

    //
    // SCIA at 9600 baud
    // @LSPCLK = 50 MHz (200 MHz SYSCLK) HBAUD = 0x02 and LBAUD = 0x8B.
    // @LSPCLK = 30 MHz (120 MHz SYSCLK) HBAUD = 0x01 and LBAUD = 0x86.


    SciaRegs.SCIHBAUD.all = 0x0000;
    SciaRegs.SCILBAUD.all = 0x0036;

    SciaRegs.SCICTL1.all = 0x0023;  // Relinquish SCI from Reset
}

//
// scia_xmit - Transmit a character from the SCI
//
void scia_xmit(int a)
{
    while (SciaRegs.SCIFFTX.bit.TXFFST != 0) {}
    SciaRegs.SCITXBUF.all =a;
}

//
// scia_msg - Transmit message via SCIA
//

void scia_16bit(uint16_t data)
{
    int first = data>>8;
    int second = data;

    scia_xmit(first);
    scia_xmit(second);
};






void scia_msg(char * msg)
{
    int i;
    i = 0;
    while(msg[i] != '\0')
    {
        scia_xmit(msg[i]);
        i++;
    }
}

//
// scia_fifo_init - Initialize the SCI FIFO
//
void scia_fifo_init()
{
    SciaRegs.SCIFFTX.all = 0xE040;
    SciaRegs.SCIFFRX.all = 0x2044;
    SciaRegs.SCIFFCT.all = 0x0;
}

void configureDAC(Uint16 dac_num)
{
    EALLOW;
    DAC_PTR[dac_num]->DACCTL.bit.DACREFSEL = REFERENCE;
    DAC_PTR[dac_num]->DACOUTEN.bit.DACOUTEN = 1;
    DAC_PTR[dac_num]->DACVALS.all = 1000;
    DELAY_US(10); // Delay for buffered DAC to power up
    EDIS;
}



//
// End of file
//
